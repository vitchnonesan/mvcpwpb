<div class="container mt-5">
<div class="card" style="width: 18rem;">
  <div class="card-body">
    <h5 class="card-title"><?= $data['blog'] ['judul'];?></h5>
    <h6 class="card-subtitle mb-2 text-body-secondary"><?= $data['blog'] ['penulis'];?></h6>
    <p class="card-text"><?= $data['blog'] ['tulisan'];?></p>
   
  </div>
</div>
</div>