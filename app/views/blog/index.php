<div class="container mt-4">
    <div class="row">
        <div class="col-6">
            <!-- Button Modal -->
        <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#formModal">
            Tambah Data Blog
        </button>

            <br>
            <br>
            <h3>Blog</h3>
            <ul class="list-group">
                    <?php foreach($data["blog"] as $b) :?>
                    <li class="list-group-item d-flex justify-content-between align-item-center">
                        <?= $b['judul'];?>
                        <a href="<?= BASE_URL;?>/blog/detail/<?= $b['id'];?>" class="badge bg-primary ">Detail</a>
                    </li>
                </ul>
                <?php endforeach ; ?>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="formModal" tabindex="-1" aria-labelledby="judulModal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title fs-5" id="judulModal">Tambah Data Blog</h1>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form action="<?= BASE_URL; ?>/blog/tambah" method="POST">

                <div class="form-group">
                    <label for="judul">Judul</label>
                    <input type="text" class="form-control" id="judul" name="judul" placeholder="Judul Blog" >
                </div>

                <div class="form-group">
                    <label for="penulis">Penulis</label>
                    <input type="text" class="form-control" id="penulis" name="penulis" placeholder="Penulis Blog" >
                </div>

                <div class="form-group">
                    <label for="tulisan">Tulisan</label>
                    <input type="text" class="form-control" id="tulisan" name="tulisan" placeholder="Tulisan Blog" >
                </div>


        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Tambah Data</button>
            </form>
      </div>
    </div>
  </div>
</div>