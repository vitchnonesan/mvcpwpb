<?php

class App {

    protected $controller = "Home";
    protected $method = "index";
    protected $params = [];

    public function __construct() {
        $url = $this->parseUrl($_SERVER);

        if (isset($url[0]) && file_exists('../app/Controller/' . $url[0] . '.php')) {
            $this->controller = $url[0];
            unset($url[0]);
        }

        require_once '../app/Controller/' . $this->controller . '.php';
        $this->controller = new $this->controller;

        if (isset($url[1])) {
            if (method_exists($this->controller, $url[1])) {
                $this->method = $url[1];
                unset($url[1]);
            }
        }
        
        $this->params = $url ? array_values($url) : [];

        call_user_func_array([$this->controller, $this->method], $this->params);
    }

    public function parseURL($url) {
        // $uri = trim($_SERVER['REQUEST_URI'],'/');
        // $script = trim($_SERVER['SCRIPT_NAME'],'/');
        // $uri = str_replace('/index.php','',$uri);
        // $script = str_replace('/index.php','',$script);
        // $uri = str_replace($script,'',$uri);
        // $url = trim($uri, '/');
        // $url = rtrim($url, '/');
        // $url = filter_var($url, FILTER_SANITIZE_URL);
        // $url = explode('/', $url);

        // var_dump(pathinfo($_SERVER['REQUEST_URI']));

        // $url = $_SERVER['REQUEST_URI'];
        // $url = trim($url, '/');
        // $url = rtrim($url, '/');
        // $url = filter_var($url, FILTER_SANITIZE_URL);
        // $url = explode('/', $url);
        // array_shift($url);
        // array_shift($url);
        
        $pathInfo = isset($_SERVER['PATH_INFO']) ? $_SERVER['PATH_INFO'] : '/';
        $url = trim($pathInfo, '/');
        $url = filter_var($url, FILTER_SANITIZE_URL);
        $url = explode('/', $url);
        return $url;

        // return $url;
    }

}


?>